import React, { Component } from 'react';
import './ComponentName.css';

class ComponentName extends Component {
	constructor(props) {
		super(props);
		//	Init state etc

		//	this.methodName = this.methodName.bind(this);
	}
		//	Methods

	render() {
		return (
			<p>ComponentName</p>
		);
	}
}

export { ComponentName };

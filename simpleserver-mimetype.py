#! /usr/local/bin/python3
# Add mime-types to SimpleHTTPServer
import sys
import os
import http.server
from http.server import HTTPServer, BaseHTTPRequestHandler
import socketserver

addr=sys.argv[1]
port=int(sys.argv[2])


Handler = http.server.SimpleHTTPRequestHandler

Handler.extensions_map={
	# -- Web content
    '.manifest'	: 'text/cache-manifest',
	'.html'		: 'text/html',
	'.xhtml'	: 'application/xhtml+xml',
	'.css'		: 'text/css',
	'.js'		: 'application/x-javascript',
	'.json'		: 'application/json',
	'.xml'		: 'application/xml',


	# ------ Media
	# -- Images
	'.gif'		: 'image/gif',
	'.ico'		: 'image/x-icon',
    '.png'		: 'image/png',
	'.jpg'		: 'image/jpg',
	'.jpeg'		: 'image/jpg',
	'.svg'		: 'image/svg+xml',
	'.webp'		: 'image/webp',

	# -- Video
	'.mp4'		: 'video/mp4',
	'.ogv'		: 'video/ogg',
	'.m4v'		: 'video/mp4',
	'.webm'		: 'video/webm',

	# -- Audio
	'.aac'		: 'audio/aac',
	'.oga'		: 'audio/ogg',
	'.weba'		: 'audio/webm',

	# -- Fonts
	'.eot'		: 'application/vnd.ms-fontobject',
	'.woff' 	: 'application/font-woff',
	'.woff2' 	: 'font/woff2',
	'.ttf'		: 'application/x-font-truetype',
	'.otf'		: 'application/x-font-opentype',

	# ------ Files
	# -- Common
	'.csv'		: 'text/csv',
	'.rtf'		: 'application/rtf',

	# -- Compressed
	'.zip'		: 'application/zip',
	'.7z'		: 'application/x-7z-compressed',
	# -- Documents
	'.pdf'		: 'application/pdf',


	# -- Default
	''			: 'application/octet-stream'
}

# Graceful exit 💅
try:
	httpd = socketserver.TCPServer((addr, port), Handler)
	print(f'🌐 serving {os.getcwd()} at http://{addr}:{port}')
	httpd.serve_forever()
except (KeyboardInterrupt, SystemExit):
	print("\n🛑 Server stopped")

##Some handy scripts to automate frequent and mindless tasks :)
Add to your path to be able to call from anywhere 
(in terminal) : 
```shell
cd path/to/this/repo [enter]
```
paste the following line into your terminal then hit enter:
```shell
./add-to-path . 'Workflow scripts, repo @ https://bitbucket.org/GazJessep/workflow/admin'
```
👍🏽 done 

📜 See individual scripts for usage
require('babel-register');
require('babel-polyfill');
const inquirer = require('inquirer');
const generateData = require('./generateData').default;

const TOOLS = {
  csv: generateData,
};

inquirer
  .prompt([
    {
      type: 'list',
      name: 'tool',
      message: 'What do you want to do?',
      choices: [{ name: 'Generate Data from CSV', value: 'csv' }],
    },
  ])
  .then((answers) => {
    TOOLS[answers.tool]();
  });

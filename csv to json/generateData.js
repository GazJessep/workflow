import { readFileSync, writeFileSync } from 'fs';
import { compact } from 'lodash';

const filename = `${__dirname}/acronyms-data.csv`;
const startLine = 2;
const duplicatesAnswers = [];
export default () => {
  const bufffer = readFileSync(filename, 'utf8');
  const lines = bufffer.split(/\r\n/);
  const getValue = (line) => {
    const [
      TPA,
      name,
      district,
      commonError1,
      commonError2,
      commonConfussion1,
      commonConfussion2,
      latitude,
      longitude,
    ] = line.split(',');
    const code = TPA.trim();
    let value;
    if (code) {
      const errors = compact([commonError1.trim(), commonError2.trim()]);
      const confused = compact([commonConfussion1.trim(), commonConfussion2.trim()]);
      if (errors[0] === code || errors[1] === code || errors[0] === errors[1]) {
        duplicatesAnswers.push(TPA);
      }
      value = {
        code: TPA,
        name,
        district: district.toLowerCase(),
        commonErrors: errors,
        commonConfussions: confused,
        latitude,
        longitude,
      };
    }
    return value;
  };

  const json = JSON.stringify(
    lines.slice(startLine).reduce((data, line) => {
      const value = getValue(line);
      return value ? [...data, value] : data;
    }, []),
  );
  writeFileSync(`${__dirname}/../src/data/acronyms-data.json`, json, 'utf8');

  console.log('duplicatesAnswers', duplicatesAnswers);
};
